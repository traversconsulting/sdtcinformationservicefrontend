import React, {Component} from "react";
import Moment from 'react-moment';

class Time extends Component {
    constructor(props) {
        super();
    }

    render() {
        var t = this.props.time;
        t = t.toString().slice(0,-3)

        return(
                <Moment unix>{t}</Moment>
        )
    }
}

export default Time