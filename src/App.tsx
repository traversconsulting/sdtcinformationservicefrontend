import React from 'react';
import './css/App.css';
import ComputerReport from './ComputerReport';
import Collapsible from "react-collapsible";

function App() {
    const hStyle = {
        backgroundColor: "#00b8e6",
        color: "white",
    }
  return (
    <div className="App">
        <h1 style={hStyle}>Computer Report</h1>
        <ComputerReport />
    </div>
  );
}


export default App;
