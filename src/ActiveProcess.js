import React, {Component} from "react";
import Collapsible from "react-collapsible";

class ActiveProcess extends Component {
    constructor(props) {
        super();
    }

    render() {
        if (this.props.process != null){
            return (
                <Collapsible trigger={"Active Process"}>
                    <p>{this.props.process.title}</p>
                    <p>{this.props.process.versionInfo}</p>
                </Collapsible>
            )
        } else {
            return (
                <p>Active Process : Unable to get information from the user's session, the agent application may be off.</p>
            )
        }
    }
}

export default ActiveProcess;