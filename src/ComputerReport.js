import Collapsible from "react-collapsible";
import React, {Component} from "react";
import Report from './Report';


class ComputerReport extends Component {
    constructor() {
        super();
        this.state = {
            computerReports: []
        }
    }

    componentDidMount() {
        fetch('http://localhost:8090/api/ComputerReport/list')
            .then(res => res.json())
            .then((data) => {
                console.log(data);
                this.setState({computerReports: data});
            })
            .catch(console.log)
    }

    displayArray() {
        return this.state.computerReports.data.map((report) => {
            return <Report report={report} />;
        });
    }

    render() {
        console.log(this.state);
        console.log(this.state.computerReports);
        const divStyle = {
            marginLeft: "20px"
        }
        if(!('data' in this.state.computerReports)) {
            return (
                <div style={divStyle}>
                    <p>No reports</p>
                </div>
            )
        }
        else{
            return (
                <div style={divStyle}>
                    {this.displayArray()}
                </div>
            )
        }
    }



}

export default ComputerReport;
