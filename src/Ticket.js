import React, {Component} from "react";

class Ticket extends Component {
    constructor(props) {
        super();
    }

    render() {
        if(this.props.ticket != null) {
            return (
                <p><br/>Issue Type: {this.props.ticket.issueType}<br/>{this.props.ticket.description}</p>
            )
        } else {return (<p></p>)}
    }
}

export default Ticket;