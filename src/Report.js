import React, {Component} from "react";
import Collapsible from "react-collapsible";
import {unstable_renderSubtreeIntoContainer} from "react-dom";
import ActiveProcess from './ActiveProcess';
import Ticket from './Ticket'
import Time from './Time';
import reportTable from './css/reportTable.css';

class Report extends Component {
    constructor(props) {
        super();
        this.state = {
            trigger: "Host Name: " + props.report.hostName
        }
    }

    render() {
        const tableStyle1 = {
            border: "1px solid black",
            borderCollapse: "collapse",
        }
        const tableStyle = {
            border: "1px solid black",
            borderCollapse: "collapse",
            marginLeft: "20px"
        }
        const pStyle = {
            marginTop: "0",
            marginBottom: "0",
            marginLeft: "20px"
        }
        const divStyle = {
            marginLeft: "20px"
        }
        function Highlight(percent) {
            if(percent >= 75) {
                return <mark style={{backgroundColor:"red"}}>{percent}</mark>
            }
            else if (percent >= 50) {
                return <mark>{percent}</mark>
            }
            else {
                return percent
            }
        }

        return(
            <div>
                <Collapsible trigger={this.state.trigger}>
                    <div style={divStyle}>
                    <table id="reportTable">
                        <tr>
                            <th>ID</th>
                            <th>Username</th>
                            <th>Date Created</th>
                            <th>Processor Usage %</th>
                            <th>Ram Usage %</th>
                        </tr>
                        <tr>
                            <td>{this.props.report.id}</td>
                            <td>{this.props.report.userName}</td>
                            <td><Time time={this.props.report.createdAt}/></td>
                            <td>{Highlight(this.props.report.processorUsagePercent)}</td>
                            <td>{Highlight(this.props.report.ramUsage)}</td>
                        </tr>
                    </table>

                    <ActiveProcess process = {this.props.report.activeProcess}/>

                    <Ticket ticket = {this.props.report.ticket}/>

                    <Collapsible trigger={"Drives:"}>
                        <table id="reportTable">
                            <tr>
                                <th>Drive</th>
                                <th>Type</th>
                                <th>File System Type</th>
                                <th>Ready</th>
                                <th>User Free Space</th>
                                <th>Total Free Space</th>
                                <th>Total Size</th>
                                <th>Percentage Used</th>
                            </tr>
                            {this.props.report.drives.map((drive) => {
                                if(drive.ready){
                                    return <tr>
                                        <td>{drive.drive}</td>
                                        <td>{drive.type}</td>
                                        <td>{drive.filesystemType}</td>
                                        <td>{drive.ready.toString()}</td>
                                        <td>{drive.userFreeSpace}</td>
                                        <td>{drive.totalFreeSpace}</td>
                                        <td>{drive.totalSize}</td>
                                        <td>{Highlight(drive.percentageUsed)}</td>
                                    </tr>
                                } else { return <tr>
                                    <td>{drive.drive}</td>
                                    <td>{drive.type}</td>
                                    <td>{drive.filesystemType}</td>
                                    <td>{drive.ready.toString()}</td>
                                    <td/>
                                    <td/>
                                    <td/>
                                    <td/>
                                </tr>

                                }

                            })}
                        </table>
                    </Collapsible>
                    <br/>

                    <Collapsible trigger={"IP Addresses:"} style={{marginLeft:"20px"}}>
                        {this.props.report.ipAddresses.map((address) => {
                            return <p style={pStyle}>{address.ipAddress}</p>
                        })}
                    </Collapsible>
                    </div>
                </Collapsible>
                <br/>
                <hr/>
            </div>

        )
}

}

export default Report;
